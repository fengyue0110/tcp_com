#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
socklen_t socket_size=sizeof(struct sockaaddr_in*);

int main()
{
    int client_socket  = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
    struct sockaddr_in server_addr;
    memset(&server_addr,0,socket_size);
    server_addr.sin_family=AF_INET;
    server_addr.sin_addr.s_addr=inet_addr("127.0.0.1");
    server_addr.sin_port=htons(1235);

    connect(client_socket,(struct sockaddr*)&server_addr,sizeof(server_addr));

    char recv_buffer[2048];
    read(client_socket,recv_buffer,sizeof(recv_buffer)-1);
    printf("Message from server : %s \n",recv_buffer);

    close(client_socket);

    return 0;
}