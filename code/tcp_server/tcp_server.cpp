#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
//#include <netinet/in.h>

socklen_t socket_size=sizeof(struct sockaddr_in);

int main()
{  
    //create server socket
    int server_socket=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
    //set server addr ip & port
    struct sockaddr_in server_addr;
    memset(&server_addr,0,socket_size);
    server_addr.sin_family=AF_INET;
    server_addr.sin_addr.s_addr=inet_addr("127.0.0.1");
    server_addr.sin_port=htons(1235);
    //bind server socket
    bind(server_socket,(struct sockaddr*)&server_addr,sizeof(server_addr));

    //set 
    listen(server_socket,10);

    struct sockaddr_in client_addr;
    int client_socket=accept(server_socket,(struct sockaddr*)&client_addr,&socket_size);

    char send_buffer[]="server send buffer!";
    write(client_socket,send_buffer,sizeof(send_buffer));

    close(server_socket);
    close(server_socket);

    return 0;
}